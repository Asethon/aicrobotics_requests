<?php
declare(strict_types=1);

namespace AicRobotics\D7\RequestsToExcel;

use Bitrix\Main\HttpApplication;
use CIBlock;

class AjaxHelper
{
    private $data = [];

    /**
     * AjaxHelper constructor.
     * @param array $data Post-данные
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function getInfoBlocks(): string
    {
        $infoBlocksInfo = CIBlock::GetList(['id' => 'desc'], ['ACTIVE' => 'Y']);
        $infoBlocks = [];
        while ($iBlock = $infoBlocksInfo->GetNext()) {
            $infoBlocks[$iBlock['ID']] = $iBlock['NAME'];
        }
        ob_end_clean();
        return json_encode($infoBlocks, JSON_UNESCAPED_UNICODE);
    }

    public function getInputs(): string
    {
        $dataMap = DataTable::getMap();

        $inputs = [];
        foreach ($dataMap as $code => $item) {
            if ($item['autocomplete']) {
                continue;
            }

            switch ($item['data_type']) {
                case 'datetime':
                    $inputType = 'date';
                    break;
                default:
                    $inputType = 'text';
                    break;
            }

            $inputs[] = [
                'code' => $code,
                'title' => $item['title'],
                'input_type' => $inputType,
                'value' => HttpApplication::getInstance()->getContext()->getRequest()->getPost($code),
            ];
        }

        return json_encode($inputs, JSON_UNESCAPED_UNICODE);
    }


}
