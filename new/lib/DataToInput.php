<?php

namespace AicRobotics\D7\RequestsToExcel;

class DataToInput
{
    private $inputs = [];
    private $dataMap = null;

    public function __construct()
    {
        $this->dataMap = DataTable::getMap();
    }

    public function getInputs($forUsing = null)
    {
        foreach ($this->dataMap as $code => $item) {
            switch ($item['data_type']) {
                case 'datetime':
                    $inputType = 'date';
                    break;
                default:
                    $inputType = 'text';
                    break;
            }

            $this->inputs[] = [
                'code' => strtolower($code),
                'type' => $inputType,
                'title' => $item['title'],
            ];
        }

        return $this->inputs;
    }
}