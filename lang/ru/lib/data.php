<?php
$MESS['DATA_ENTITY_ID_FIELD'] = "ID";
$MESS['DATA_ENTITY_CREATED_FIELD'] = "Дата создания выгрузки";
$MESS['DATA_ENTITY_TITLE_FIELD'] = "Заголовок";
$MESS['DATA_ENTITY_DATE_START_FIELD'] = "Диапазон выгрузки (от)";
$MESS['DATA_ENTITY_DATE_END_FIELD'] = "Диапазон выгрузки (до)";
$MESS['DATA_ENTITY_PATH_FIELD'] = "Файл выгрузки";


### Текст "ядра" модуля ###
$MESS['MODULE_NAME'] = "Создание excel-выгрузки";
$MESS['MODULE_DESCRIPTION'] = "Предназначен для создания выгрузки заявок";