<?php

namespace AicRobotics\D7\RequestsToExcel;

use Bitrix\Main\Entity;

class DataTable extends Entity\DataManager
{
    public static function getTableName()
    {
        return 'aicrobotics_excel';
    }

    public static function getMap()
    {
        return [
            'ID' => [
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => "ID",
            ],
            'TITLE' => [
                'data_type' => 'string',
                'required' => true,
                'title' => "Заголовок",
            ],
            'CREATED' => [
                'data_type' => 'datetime',
                'title' => "Дата создания выгрузки",
            ],
            'DATE_START' => [
                'data_type' => 'datetime',
                'title' => "Диапазон выгрузки (от)",
            ],
            'DATE_END' => [
                'data_type' => 'datetime',
                'title' => "Диапазон выгрузки (до)",
            ],
            'PATH' => [
                'data_type' => 'string',
                'title' => "Файл выгрузки",
            ],
        ];
    }
}