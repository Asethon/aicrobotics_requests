<?php

namespace AicRobotics\D7\RequestsToExcel;

use CIBlockElement;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Excel
{
    private $spreadSheet;
    private $properties = [];
    private $nextLineId = 1;

    public function __construct($properties)
    {
        $this->spreadSheet = new Spreadsheet();
        $this->properties = $properties;
    }

    public function writeToSheet($infoBlocks, $sectionId, $dateStart, $dateEnd)
    {
        $arFilter = [
            "IBLOCK_ID" => $infoBlocks,
            "SECTION_ID" => $sectionId,
            "ACTIVE" => 'Y',
        ];

        foreach ($infoBlocks as $infoBlockId) {
            $this->setCellHeaders($infoBlockId);
            $selectFields = json_decode($this->properties[$infoBlockId]);
            foreach ($selectFields as &$field) {
                $field = 'PROPERTY_' . $field;
            }
            $selectFields[] = 'NAME';
            $selectFields[] = 'DATE_CREATE';
            $res = CIBlockElement::GetList(
                [], $arFilter, false, false,
                $selectFields
            );
            while ($ob = $res->GetNextElement()) {
                $arFields = $ob->GetFields();
                if (strtotime($ob->GetFields()['DATE_CREATE']) < strtotime($dateStart) || strtotime($ob->GetFields()['DATE_CREATE']) > strtotime($dateEnd)) {
                    //continue;
                }
                $symbol = 'A';

                foreach ($selectFields as $field) {
                    echo '<pre>' . print_r($ob->GetProperty(str_replace('PROPERTY_', '', $field)), true) . '</pre>';
                    $text = strip_tags(str_replace('<br>', PHP_EOL . '   ', $arFields[$field . '_VALUE']));
                    $this->getSheet()->setCellValue($symbol . $this->nextLineId, $text);
                    if ($symbol === 'Z') {
                        $symbol = 'A';
                    }
                    $symbol++;
                }
                $this->nextLineId++;
            }
        }
    }

    /**
     * @return Worksheet|string
     */
    public function getSheet()
    {
        try {
            return $this->spreadSheet->getActiveSheet();
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    private function setCellHeaders($id)
    {
        $symbol = 'A';
        foreach (json_decode($this->properties[$id]) as $property) {
            $this->getSheet()->setCellValue($symbol . $this->nextLineId, $property);
            $symbol++;
        }
        $this->nextLineId++;
    }

    public function writeToExcel($filename)
    {
        $writer = new Xlsx($this->spreadSheet);
        if (file_exists('/')) {
            try {
                $writer->save($_SERVER['DOCUMENT_ROOT'] . '/upload/aicrobotics.requests/' . $filename . '.xlsx');
            } catch (\PhpOffice\PhpSpreadsheet\Writer\Exception $e) {
                return $e->getMessage();
            }
        }

        return true;
    }
}
