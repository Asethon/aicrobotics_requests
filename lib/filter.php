<?php
namespace AicRobotics\D7\RequestsToExcel;


use Bitrix\Main\Type\DateTime;

class Filter {
    private $id;
    private $title;
    private $created;
    private $dateStart;
    private $dateEnd;
    private $path;

    /**
     * Filter constructor.
     * @param integer $id
     * @param string $title
     * @param DateTime $created
     * @param DateTime $dateStart
     * @param DateTime $dateEnd
     * @param string $path
     */
    public function __construct($id, $title, $created, $dateStart, $dateEnd, $path)
    {
        $this->setId($id);
        $this->setTitle($title);
        $this->setCreated($created);
        $this->setDateStart($dateStart);
        $this->setDateEnd($dateEnd);
        $this->setPath($path);
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param integer $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return DateTime
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * @param DateTime $dateStart
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;
    }

    /**
     * @return DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * @param DateTime $dateEnd
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }


}