<?php

namespace AicRobotics\D7\RequestsToExcel;

use Bitrix\Main\ArgumentException;
use Bitrix\Main\ObjectException;
use Bitrix\Main\Type\DateTime;
use Exception;

class Requests
{
    private $properties;
    private $values;

    /**
     * @param array $values
     * @return int
     * @throws ArgumentException
     * @throws ObjectException
     * @throws Exception
     */
    public function create(array $values)
    {
        if (empty($values)) {
            throw new ArgumentException('Поля не заполнены');
        }

        if (empty($values['title'])) {
            throw new ArgumentException('Поле "заголовок" не заполнено');
        }

        if (empty($values['date_start'])) {
            throw new ArgumentException('Поле "дата выгрузки от" не заполнено');
        }

        if (empty($values['date_end'])) {
            throw new ArgumentException('Поле "дата выгрузки до" не заполнено');
        }

        if (empty($values['iBlocks'])) {
            throw new ArgumentException('Не выбран ни один инфоблок');
        }

        if (empty($values['sections'])) {
            throw new ArgumentException('Не выбран ни один раздел');
        }

        if (empty($values['properties'])) {
            throw new ArgumentException('Не выбрано ни одно свойство');
        }

        $this->prepareValues($values);

        $this->values['PATH'] = 'export-' . $this->values['DATE_START']->toString() . '-to-' . $this->values['DATE_END']->toString();
        $excel = new Excel($this->properties);
        $excel->writeToSheet($this->values['IBLOCKS'], $this->values['SECTIONS'], $this->values['DATE_START'],
            $this->values['DATE_END']);
        unset($this->values['SECTIONS']);
        unset($this->values['IBLOCKS']);

        try {
            DataTable::add($this->values);
            $excel->writeToExcel($this->values['PATH']);
        } catch (Exception $e) {
            echo 'Add-error: ' . $e->getMessage();
        }

        return 0;
    }

    /**
     * @param $id
     * @param $values
     * @return int
     * @throws Exception
     */
    public function edit($id, $values)
    {
        $this->prepareValues($values);
        DataTable::update($id, $this->values);

        return 0;
    }

    /**
     * @param array $values
     * @throws ObjectException
     * @throws \Exception
     */
    private function prepareValues(array $values)
    {
        $this->properties = $values['properties'];

        unset($values['properties']);
        foreach ($values as $key => $value) {
            $this->values[strtoupper($key)] = $value;
        }

        $this->values['DATE_START'] = (new DateTime((new \DateTime($this->values['DATE_START']))->format('d.m.Y H:i:s')));
        $this->values['DATE_END'] = (new DateTime((new \DateTime($this->values['DATE_END']))->format('d.m.Y H:i:s')));
        $this->values['CREATED'] = (new DateTime((new \DateTime())->format('d.m.Y H:i:s')));
    }

    public static function delete($id)
    {
        try {
            DataTable::delete($id);
        } catch (Exception $e) {
            return $e->getMessage();
        }

        return true;
    }

    public static function getAll($offset = 0, $limit = 20)
    {
        try {
            $result = DataTable::getList(
                [
                    'select' => array('*'),
                    'limit' => $limit,
                    'offset' => $offset,
                ]);
        } catch (ArgumentException $e) {
            return $e->getMessage();
        }

        $row = $result->fetchAll();
        return $row;
    }

    public static function getById(int $id = 1)
    {
        $result = DataTable::getById($id);
        $row = $result->fetch();
        return $row;
    }

    public static function getByFilter(array $filter, int $offset = 0, int $limit = 20)
    {

        foreach ($filter as $key => $item) {
            if (is_array($item)) {
                $filterDate[$key] = $item;
                unset($filter[$key]);
            }
        }

        try {
            $result = DataTable::getList([
                'filter' => $filter,
                'limit' => $limit,
                'offset' => $offset,
            ]);
            if (!empty($filterDate)) {
                $items = $result->fetchAll();
                foreach ($filterDate as $code => $date) {
                    foreach ($items as $id => $item) {
                        if (strtotime($item[$code]) < strtotime($date[0]) || strtotime($item[$code]) > strtotime($date[1])) {
                            unset($items[$id]);
                        }
                    }
                }
                return $items;
            }

        } catch (ArgumentException $e) {
            return $e->getMessage();
        }
        $row = $result->fetchAll();
        return $row;
    }
}