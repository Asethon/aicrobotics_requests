<?php

namespace AicRobotics\D7\RequestsToExcel;

use Bitrix\Main\ArgumentException;

class Requests
{
    public static function getAll($offset = 0, $limit = 20)
    {
        try {
            $result = DataTable::getList(
                [
                    'select' => array('*'),
                    'limit' => $limit,
                    'offset' => $offset,
                ]);
        } catch (ArgumentException $e) {
            return $e->getMessage();
        }

        $row = $result->fetch();
        return $row;
    }

    public static function getById(int $id = 1)
    {
        $result = DataTable::getById($id);
        $row = $result->fetch();
        return $row;
    }

    public static function getByFilter(array $filter, int $offset = 0, int $limit = 20)
    {
        try {
            $result = DataTable::getList([
                'filter' => $filter,
                'limit' => $limit,
                'offset' => $offset,
            ]);

        } catch (ArgumentException $e) {
            return $e->getMessage();
        }
        $row = $result->fetchAll();
        return $row;
    }
}