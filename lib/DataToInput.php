<?php

namespace AicRobotics\D7\RequestsToExcel;

class DataToInput
{
    private $inputs = [];
    private $dataMap = null;

    public function __construct()
    {
        $this->dataMap = DataTable::getMap();
    }

    public function getInputs($forUsing = null)
    {
        foreach ($this->dataMap as $code => $item) {
            if (($forUsing === 'add' || $forUsing === 'edit') && ($code === 'ID' || $code === 'CREATED' || $code === 'PATH')) {
                continue;
            }

            switch ($item['data_type']) {
                case 'integer':
                    $inputType = 'text';
                    break;
                case 'string':
                    $inputType = 'text';
                    if ($code === 'PATH') {
                        $inputType = 'file';
                    }
                    break;
                case 'datetime':
                    $inputType = 'date';
                    break;
                default:
                    $inputType = 'text';
                    break;
            }

            $this->inputs[] = [
                'code' => strtolower($code),
                'type' => $inputType,
                'title' => $item['title'],
            ];
        }

        return $this->inputs;
    }

    /**
     * @return array|null
     */
    public function getDataMap(): array
    {
        return $this->dataMap;
    }
}