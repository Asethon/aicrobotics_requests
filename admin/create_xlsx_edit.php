<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/modules/aicrobotics.requests/admin/create_xlsx_admin.php');

use AicRobotics\D7\RequestsToExcel\DataTable;
use AicRobotics\D7\RequestsToExcel\DataToInput;
use AicRobotics\D7\RequestsToExcel\Requests;
use Bitrix\Main\ArgumentException;
use Bitrix\Main\HttpApplication;
use Bitrix\Main\ObjectException;
use Bitrix\Main\SystemException;

if (!CModule::IncludeModule('aicrobotics.requests')) {
    return 'Ошибка: модуль не подключён';
}

try {
    $request = HttpApplication::getInstance()->getContext()->getRequest();
} catch (SystemException $e) {
    echo $e->getMessage();
}

if (empty($request->get('ID'))) {
    header('Location: ./create_xlsx_list.php');
} elseif ($request->get('delete') == true) {
    Requests::delete($request->get('ID'));
    header('Location: ./create_xlsx_list.php');
}

$sTableID = DataTable::getTableName();
$oSort = new CAdminSorting($sTableID, "ID", "desc");
$listAdmin = new CAdminList($sTableID, $oSort);
$inputs = (new DataToInput())->getInputs('edit');

$requestItem = Requests::getById($request->get('ID'));
if ($request->isPost()) {
    try {
        (new Requests)->edit($requestItem['ID'], $request->getPostList()->toArray());
    } catch (Exception $e) {
        $errors[] = $e->getMessage();
    }
    $requestItem = Requests::getById($request->get('ID'));
} else {
    $requestItem['DATE_START'] = $requestItem['DATE_START']->format('Y-m-d');
    $requestItem['DATE_END'] = $requestItem['DATE_END']->format('Y-m-d');
}
?>

<div class="xls-block">
    <div class="xls-block-filters">
        <h2>Изменение элемента: <?= $requestItem['TITLE'] ?></h2>
        <?php foreach ($errors as $error): ?>
            <p style="color: rgba(244,0,14,0.82)"><?= $error ?></p>
        <?php endforeach; ?>
        <form name="add_form" method="post" action="<?= $APPLICATION->GetCurPage() ?>?ID=<?= $requestItem['ID'] ?>">
            <?php foreach ($inputs as $input): ?>
                <div class="xls-block-filters-input col-xs-6 col-sm-3">
                    <?php if ($input['type'] === 'date'): ?>
                        <label for="<?= $input['code'] ?>"><?= $input['title'] ?>:<br></label>
                    <?php endif; ?>
                    <input class="xls-filter" type="<?= $input['type'] ?>" id="<?= $input['code'] ?>"
                           name="<?= $input['code'] ?>"
                           value="<?= $requestItem[strtoupper($input['code'])]; ?>"
                           placeholder="<?= $input['title'] ?>">
                </div>
            <?php endforeach; ?>
            <input type="submit" value="Сохранить">
        </form>
    </div>
</div>