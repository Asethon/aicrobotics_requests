<?php
defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();
if (!$USER->IsAuthorized()) {
    return false;
}

$aMenu = array(
    "parent_menu" => "global_menu_services",
    "sort" => 10,
    "text" => "Создание excel-выгрузки",
    "title" => "Предназначен для создания выгрузки заявок",
    "icon" => "",
    'url' => 'create_xlsx_list.php?lang=ru&table_name=aicrobotics_excel',
    "page_icon" => "",
    "module_id" => "aicrobotics.requests",
    "items_id" => "sync_reference",
    "items" => [],
);

$aMenu["items"][] = [
    "text" => "Excel-выгрузки",
    "url" => "create_xlsx_list.php?lang=" . LANGUAGE_ID,
    "more_url" => [
        "create_xlsx_add.php",
        "create_xlsx_edit.php",
    ],
    "title" => "Excel-выгрузки",
];
/*$aMenu["items"][] = [
    "text" => "Excel-выгрузки: Редактирование элемента",
    "url" => "create_xlsx_edit.php?lang=" . LANGUAGE_ID,
    "more_url" => [
        "create_xlsx_edit.php"
    ],
];*/
return $aMenu;