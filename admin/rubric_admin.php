<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/modules/aicrobotics.requests/include.php');

$POST_RIGHT = $APPLICATION->GetGroupRight("aicrobotics.requests");
if ($POST_RIGHT == "D") {
    $APPLICATION->AuthForm("Доступ запрещён");
}

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");