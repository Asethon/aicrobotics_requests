<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/modules/aicrobotics.requests/include.php');

use Bitrix\Main\Page\Asset;
Asset::getInstance()->addJs('https://cdn.jsdelivr.net/npm/vue');
Asset::getInstance()->addJs('https://unpkg.com/axios/dist/axios.min.js');

Asset::getInstance()->addJs($_SERVER['DOCUMENT_ROOT'] . '/local/modules/aicrobotics.requests/new/lib/js/main.js');
Asset::getInstance()->addCss($_SERVER['DOCUMENT_ROOT'] . '/local/common/js/bootstrap/css/bootstrap.css');
Asset::getInstance()->addCss($_SERVER['DOCUMENT_ROOT'] . '/local/modules/aicrobotics.requests/admin/css/main.css');
$POST_RIGHT = $APPLICATION->GetGroupRight("aicrobotics.requests");
if ($POST_RIGHT == "D") {
    $APPLICATION->AuthForm("Доступ запрещён");
}
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

?>
    <style>
        <?php require_once($_SERVER['DOCUMENT_ROOT'] . '/local/modules/aicrobotics.requests/admin/css/main.css')?>
    </style>