<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/modules/aicrobotics.requests/admin/create_xlsx_admin.php');

use Bitrix\Main\HttpApplication;
use Bitrix\Main\SystemException;
use Symfony\Component\Yaml\Yaml;

if (!CModule::IncludeModule('aicrobotics.requests')) {
    return 'Ошибка: модуль не подключён';
}

try {
    $request = HttpApplication::getInstance()->getContext()->getRequest();
} catch (SystemException $e) {
    echo $e->getMessage();
}

if ($request->getRequestMethod() === 'POST') {
    if (!CModule::IncludeModule('aicrobotics.requests')) {
        return json_encode('Ошибка: модуль не подключён', JSON_UNESCAPED_UNICODE);
    }
    $data = (array)json_decode(file_get_contents('php://input'));
    $routes = Yaml::parseFile($_SERVER['DOCUMENT_ROOT'] . '/local/modules/aicrobotics.requests/new/lib/route.yaml');
    $route = $routes[key($data)];
    $controller = 'AicRobotics\\D7\\RequestsToExcel\\' . $route['controller'];
    $helper = new $controller($request->getPostList()->toArray());
    $result = [];
    foreach ($data['get'] as $getFunction) {
        $result[$getFunction] = $helper->{$route['routes'][$getFunction]}();
    }
    ob_end_clean();
    header('Content-Type: application/json');
    echo json_encode($result, JSON_UNESCAPED_UNICODE);
    die();
}
?>

<div class="xls-block">
    <div class="xls-block-filters">
        <h2>Создание выгрузки</h2>
        <?php foreach ($errors as $error): ?>
            <p style="color: rgba(244,0,14,0.82)"><?= $error ?></p>
        <?php endforeach; ?>
        <form id="add" method="post" action="<?= $APPLICATION->GetCurPage() ?>">
            <div class="xls-block-filters-input col-xs-6 col-sm-3" id="inputs" v-html="inputs_view">
            </div>
            <div class="blocks" data-step="1">
                <label for="info_blocks">Выберите инфоблок(-и):</label>
                <br>
                <select multiple name="info_blocks[]" id="info_blocks">
                </select>
                <br>
                <br>
                <input type="button" onclick="next();" value="Далее">
            </div>
            <div class="sections" style="display: none;" data-step="2">
                <label for="sections">Выберите раздел(-ы):</label>
                <select multiple name="sections[]" id="sections">
                </select>
                <br>
                <br>
                <input type="button" onclick="prev();" value="Назад">
                <input type="button" onclick="next();" value="Далее">
            </div>
            <div class="properties" style="display:none;" data-step="3">
                <label for="properties">Выберите свойства:</label>
                <br>
                <select multiple name="properties[]" id="properties">
                </select>
                <br>
                <br>
                <input type="button" onclick="prev();" value="Назад">
                <input type="submit" value="Создать выгрузку">
            </div>
        </form>
    </div>
</div>
<script>
    window.addEventListener("load", function (event) {
        let formAdd = new Vue({
            el: '#add',
            data() {
                return {
                    input: {
                        code: null,
                        input_type: null,
                        title: null,
                        value: '',
                    },
                    inputs: [],
                    inputs_view: '',
                }
            },
            mounted: function () {
                let payload = {
                    get: ['inputs']
                };

                axios
                    .post('', payload)
                    .then((response) => {
                        let inputs = JSON.parse(response.data.inputs);
                        for (let i = 0; i < inputs.length; i++) {
                            let input = this.input;
                            input.code = inputs[i]['code'];
                            input.input_type = inputs[i]['input_type'];
                            input.title = inputs[i]['title'];
                            input.value = inputs[i]['value'];
                            this.inputs.push(input);
                        }

                        for (let i = 0; i < this.inputs.length; i++) {
                            if (this.inputs[i].input_type === 'date') {
                                this.inputs_view += '<label for="' + this.inputs[i].code + '">' + this.inputs[i].title + '</label>';
                            }
                            this.inputs_view += '<input class="xls-filter" type="' + this.inputs[i].input_type + '" ' +
                                'name="' + this.inputs[i].code + '" value="' + this.inputs[i].value + '" placeholder="' + this.inputs[i].title + '">';
                        }

                    })
                    .catch((error) => {
                        console.log(error);
                    });
            },
        });
    });
</script>