<?php
use AicRobotics\D7\RequestsToExcel\DataTable;
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
if (CModule::IncludeModule('aicrobotics.requests')) {
    global $listAdmin;
    $sTableID = DataTable::getTableName();
    $oSort = new CAdminSorting($sTableID, "ID", "desc");
    $listAdmin = new CAdminList($sTableID, $oSort);
    $listAdmin->BeginCustomContent();
} else {
    echo 'Ошибка подключения модуля';
}