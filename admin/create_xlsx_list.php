<?php
require_once($_SERVER['DOCUMENT_ROOT'] . '/local/modules/aicrobotics.requests/admin/create_xlsx_admin.php');

use AicRobotics\D7\RequestsToExcel\DataTable;
use AicRobotics\D7\RequestsToExcel\Requests;
use Bitrix\Main\HttpApplication;
use Bitrix\Main\SystemException;


if (!CModule::IncludeModule('aicrobotics.requests')) {
    return 'Ошибка: модуль не подключён';
}

$map = DataTable::getMap();

$inputs = [];

$inputType = 'text';
foreach ($map as $code => $item) {
    switch ($item['data_type']) {
        case 'integer':
            $inputType = 'text';
            $findMaskEnd = '';
            break;
        case 'string':
            $inputType = 'text';
            $findMaskEnd = '%';
            if ($code === 'PATH') {
                $inputType = 'file';
            }
            break;
        case 'datetime':
            $inputType = 'date';
            $findMaskEnd = '';
            break;
        default:
            $inputType = 'text';
            $findMaskEnd = '%';
            break;
    }

    $inputs[] = [
        'code' => 'find_' . strtolower($code),
        'type' => $inputType,
        'title' => $item['title'],
        'original_code' => $code,
        'find_mask_end' => $findMaskEnd
    ];
}

try {
    $request = HttpApplication::getInstance()->getContext()->getRequest();
} catch (SystemException $e) {
    echo $e->getMessage();
}

if ($request->isPost()) {
    $filter = [];
    foreach ($inputs as $input) {
        if (!empty($request->getPost($input['code']))) {
            $value = $request->getPost($input['code']);
            if ($input['type'] == 'date') {
                $value = (new \DateTime($value))->format("d-m-Y H:i:s");
                $filter[$input['original_code']] = [
                    $value,
                    date("d-m-Y H:i:s", strtotime($value) + 60 * 60 * 24)
                ];
                continue;
            }
            $filter[$input['original_code']] = $value . $input['find_mask_end'];
        }
    }
    $requestsList = Requests::getByFilter($filter);
} else {
    $requestsList = Requests::getAll();
}

?>
<div class="xls-block">
    <div class="xls-block-filters">
        <h2>Фильтр</h2>
        <form name="find_form" method="post" action="<?= $APPLICATION->GetCurPage() ?>">
            <input type="hidden" name="lang" value="<?= LANG ?>">
            <?php foreach ($inputs as $input): ?>
                <div class="xls-block-filters-input col-xs-6 col-sm-3">
                    <?php if ($input['type'] == 'file'):
                        continue;
                    endif; ?>
                    <?php if ($input['type'] === 'date'): ?>
                        <label for="<?= $input['code'] ?>"><?= $input['title'] ?>:<br></label>
                    <?php endif; ?>
                    <input class="xls-filter" type="<?= $input['type'] ?>" id="<?= $input['code'] ?>"
                           name="<?= $input['code'] ?>"
                           value="<?= $request->getPost($input['code']) ?>"
                           placeholder="<?= $input['title'] ?>">
                </div>
            <?php endforeach; ?>
            <input type="submit" value="Поиск">
        </form>
    </div>
    <a href="create_xlsx_add.php">Добавить</a>
    <br>
    <div class="adm-list-table-layout">
        <div class="adm-list-table-wrap adm-list-table-without-footer">
            <table class="adm-list-table">
                <thead>
                <tr class="adm-list-table-header">
                    <td class="adm-list-table-cell adm-list-table-popup-block">
                        <div class="adm-list-table-cell-inner"></div>
                    </td>
                    <?php foreach ($inputs as $input): ?>
                        <td class="adm-list-table-cell adm-list-table-cell-sort">
                            <div class="adm-list-table-cell-inner">
                                <?= $input['title']; ?>
                            </div>
                        </td>
                    <?php endforeach; ?>
                </tr>
                </thead>
                <tbody>
                <?php if (!empty($requestsList)): ?>
                    <?php foreach ($requestsList as $request): ?>
                        <tr class="adm-list-table-row item-block" data-item-id="<?= $request['ID'] ?>">
                            <td class="adm-list-table-cell adm-list-table-popup-block item-menu">
                                <div class="adm-list-table-popup" title="Действия"></div>
                            </td>
                            <?php foreach ($request as $code => $value): ?>
                                <td class="adm-list-table-cell align-right">
                                    <?php if ($code === 'PATH'): ?>
                                        <a href="/upload/aicrobotics.requests/<?= $value?>.xlsx">Скачать</a>
                                    <?php else: ?>
                                        <?= $value ?>
                                    <?php endif; ?>
                                </td>
                            <?php endforeach; ?>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr>
                        <td colspan="7" class="adm-list-table-cell adm-list-table-empty">- Нет данных -</td>
                    </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>

    <div class="bx-core-popup-menu bx-core-popup-menu-bottom bx-core-popup-menu-level0 item-menu-block"
         style="z-index: 1000; position: absolute; display: none; height: auto; width: auto;">
        <span class="bx-core-popup-menu-angle" style="left: 11px;"></span>
        <span class="bx-core-popup-menu-item bx-core-popup-menu-item-default item-menu-block-edit" title="Изменить">
        <span class="bx-core-popup-menu-item-icon adm-menu-edit"></span>
        <span class="bx-core-popup-menu-item-text">Изменить</span>
    </span>
        <span class="bx-core-popup-menu-item item-menu-block-delete" title="Удалить">
        <span class="bx-core-popup-menu-item-icon adm-menu-delete"></span>
        <span class="bx-core-popup-menu-item-text">Удалить</span></span>
    </div>
</div>
<script>
    $(document).ready(function () {
        let menu = $(".item-menu");
        let menuBlock = $(".item-menu-block");
        let itemId = null;
        menu.click(function (e) {

            let relativeX = e.pageX;
            let relativeY = e.pageY;
            itemId = $(this).parent(".item-block").data('item-id');
            menuBlock.css({display: 'block', top: relativeY, left: relativeX - 20});
        });

        $('.item-menu-block-edit').click(function () {
            $(location).attr('href', './create_xlsx_edit.php?LANGUAGE_ID=' + phpVars['LANGUAGE_ID'] + '&ID=' + itemId);
        });

        $('.item-menu-block-delete').click(function () {
            $(location).attr('href', './create_xlsx_edit.php?LANGUAGE_ID=' + phpVars['LANGUAGE_ID'] + '&ID=' + itemId + '&delete=true');
        });

        $(".item-block").dblclick(function () {
            if (itemId === null) {
                itemId = $(this).data('item-id');
            }
            $(location).attr('href', './create_xlsx_edit.php?LANGUAGE_ID=' + phpVars['LANGUAGE_ID'] + '&ID=' + itemId);
        });

        $(document).mouseup(function (e) { // событие клика по веб-документу
            if (!menuBlock.is(e.target) // если клик был не по нашему блоку
                && menuBlock.has(e.target).length === 0) { // и не по его дочерним элементам
                menuBlock.hide(); // скрываем его
            }
        });
    });
</script>