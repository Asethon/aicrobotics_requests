CREATE TABLE `aicrobotics_excel`(
    `ID` INT(11) NOT NULL AUTO_INCREMENT,
    `CREATED` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    `TITLE` VARCHAR(256) NOT NULL,
    `DATE_START` TIMESTAMP,
    `DATE_END` TIMESTAMP,
    `PATH` VARCHAR(256),
    PRIMARY KEY(ID)
);