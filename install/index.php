<?php

use Bitrix\Main\EventManager;

defined('B_PROLOG_INCLUDED') and (B_PROLOG_INCLUDED === true) or die();

class aicrobotics_requests extends CModule
{
    public $MODULE_ID = "aicrobotics.requests";
    public $MODULE_VERSION;
    public $MODULE_VERSION_DATE;
    public $MODULE_NAME;
    public $MODULE_DESCRIPTION;
    public $errors;
    public $PARTNER_NAME = "AicRobotics";
    public $PARTNER_URI = "https://aicrobotics.ru";

    function __construct()
    {
        //$arModuleVersion = array();
        $this->MODULE_VERSION = "1.0.0";
        $this->MODULE_VERSION_DATE = "22.07.2019";
        $this->MODULE_NAME = "Создание excel-выгрузки";
        $this->MODULE_DESCRIPTION = "Предназначен для создания выгрузки в формате excel-файла";
    }

    public function DoInstall()
    {
        $this->InstallDB();
        $this->InstallFiles();
        RegisterModule($this->MODULE_ID);
        return true;
    }

    function DoUninstall()
    {
        $this->UnInstallDB();
        $this->UnInstallFiles();
        UnRegisterModule($this->MODULE_ID);
        return true;
    }

    function InstallDB()
    {
        global $DB;
        $this->errors = false;
        $this->errors = $DB->RunSQLBatch($_SERVER['DOCUMENT_ROOT'] . "/local/modules/" . $this->MODULE_ID . "/install/db/install.sql");
        if (!$this->errors) {
            return true;
        } else {
            return $this->errors;
        }
    }

    function UnInstallDB()
    {
        global $DB;
        $this->errors = false;
        $this->errors = $DB->RunSQLBatch($_SERVER['DOCUMENT_ROOT'] . "/local/modules/" . $this->MODULE_ID . "/install/db/uninstall.sql");
        if (!$this->errors) {
            return true;
        } else {
            return $this->errors;
        }
    }

    public function InstallFiles()
    {
        copy($_SERVER['DOCUMENT_ROOT'] . '/local/modules/' . $this->MODULE_ID . '/admin/create_xlsx_add.php', $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin/create_xlsx_add.php');
        copy($_SERVER['DOCUMENT_ROOT'] . '/local/modules/' . $this->MODULE_ID . '/admin/create_xlsx_list.php', $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin/create_xlsx_list.php');
        copy($_SERVER['DOCUMENT_ROOT'] . '/local/modules/' . $this->MODULE_ID . '/admin/create_xlsx_edit.php', $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin/create_xlsx_edit.php');
    }

    public function UnInstallFiles()
    {
        unlink($_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin/create_xlsx_add.php');
        unlink($_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin/create_xlsx_list.php');
        unlink($_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin/create_xlsx_edit.php');
    }
}