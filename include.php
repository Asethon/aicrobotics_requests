<?php
use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;

try {
    Loader::registerAutoloadClasses(
        "aicrobotics.requests",
        array(
            "AicRobotics\\D7\\RequestsToExcel\\RequestService" => "new/lib/RequestService.php",
            "AicRobotics\\D7\\RequestsToExcel\\AjaxHelper" => "new/lib/AjaxHelper.php",
            "AicRobotics\\D7\\RequestsToExcel\\DataTable" => "lib/DataTable.php",
            "AicRobotics\\D7\\RequestsToExcel\\DataToInput" => "lib/DataToInput.php",
            "AicRobotics\\D7\\RequestsToExcel\\Excel" => "lib/Excel.php",
        )
    );
} catch (LoaderException $e) {
    return $e->getMessage();
}